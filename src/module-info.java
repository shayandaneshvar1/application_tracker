module application.tracker {
//    exports main.java.view;
//    exports main.java.repository;
//    exports main.java.model;
//    exports main.java.controller;

    requires javafx.media;
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.base;
    requires javafx.fxml;
    requires javafx.web;
    requires javafx.swing;
    requires javafx.swt;

    requires com.jfoenix;

    requires java.sql;

    requires itextpdf;
    requires opencsv;

    requires java.mail;
    requires lib.activation;

//    requires org.junit.jupiter.api;

    opens main.java.view;
    opens main.java.repository;
    opens main.java.model;
    opens main.java.controller;
    opens main.resources;

    opens test.java;

}