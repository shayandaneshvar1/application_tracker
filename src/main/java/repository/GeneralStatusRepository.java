package main.java.repository;

import main.java.model.Event;
import main.java.model.GeneralStatus;
import main.java.model.State;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class GeneralStatusRepository extends RepositoryBase
        implements IRepository<GeneralStatus>, SearchRepository<GeneralStatus>, GetPrimaryKey<GeneralStatus> {
    private PreparedStatement getByAppID;
    private PreparedStatement fetchIdPrimeryKey;

    public GeneralStatusRepository(Connection connection) {
        super(connection);
        prepareStatements();
    }

    @Override
    protected void prepareStatements() {

        try {
            updateStatement = connection.prepareStatement(
                    "update generalStatus set  status = ?, app_id = ?," +
                            " creation_date = ?, private_description = ? , " +
                            "public_description=? where id = ? ;"
            );
            insertStatement = connection.prepareStatement("insert into generalStatus values (null,?,?,?,?,?)");
            selectAllStatement = connection.prepareStatement(
                    "select * from all "
            );
            removeStatement = connection.prepareStatement(
                    "delete from generalStatus where id=?"
            );
            selectRowStatement = connection.prepareStatement(
                    "select * from generalStatus where id=? "
            );
            searchStatement = connection.prepareStatement("SELECT * FROM hire" +
                    ".generalStatus " +
                    "WHERE status like ?");
            getByAppID = connection.prepareStatement("SELECT * FROM hire" +
                    ".generalStatus where app_id = ?");

            fetchIdPrimeryKey = connection.prepareStatement("SELECT hire.generalStatus.id FROM hire.generalStatus " +
                    "WHERE hire.generalStatus.status = ? " +
                    "AND hire.generalStatus.app_id = ? " +
                    "AND hire.generalStatus.creation_date = ? " +
                    "AND hire.generalStatus.private_description = ? " +
                    "AND hire.generalStatus.public_description = ? ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<GeneralStatus> selectAll() {
        return fetchData(selectAllStatement);
    }

    @Override
    public int insert(GeneralStatus generalStatus) {
        try {
            insertStatement.setString(1, generalStatus.getState().toString());
            insertStatement.setInt(2, generalStatus.getAppId());
            insertStatement.setString(3, generalStatus.getDate());
            insertStatement.setString(4, generalStatus.getPrivateDescriptions());
            insertStatement.setString(5, generalStatus.getPublicDescriptions());
            insertStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public List<GeneralStatus> selectByAppID(Integer id) {
        try {
            getByAppID.setInt(1, id);
            return fetchData(getByAppID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assert false;//fixme
        return null;
    }

    @Override
    public int delete(Integer t) {
        try {
            removeStatement.setInt(1, t);
            return removeStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public GeneralStatus selectRow(int id) {
        try {
            selectRowStatement.setInt(1, id);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return fetchData(selectRowStatement).size() > 0 ? fetchData(selectRowStatement).get(0) : null;
    }

    @Override
    public int update(GeneralStatus generalStatus) {
        try {
            updateStatement.setString(1, generalStatus.getState().toString());
            updateStatement.setInt(2, generalStatus.getAppId());
            updateStatement.setString(3, generalStatus.getDate());
            updateStatement.setString(4, generalStatus.getPrivateDescriptions());
            updateStatement.setString(5, generalStatus.getPublicDescriptions());
            updateStatement.setInt(6,generalStatus.getID());
            return updateStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<GeneralStatus> fetchData(PreparedStatement preparedStatement) {
        List<GeneralStatus> statuses = new ArrayList<>();
        try {

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                GeneralStatus generalStatus = new GeneralStatus(
                        resultSet.getInt(1),
                        State.getValue(resultSet.getString(2)),
                        resultSet.getInt(3),
                        Event.formatDate(resultSet.getString(4)),
                        resultSet.getString(5),
                        resultSet.getString(6));
                statuses.add(generalStatus);
            }
            return statuses;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assert false;
        return null;
    }

    @Override
    public List<GeneralStatus> search(String... str) {
        try {
            searchStatement.setString(1, "%" + str[0] + "%");
            return fetchData(searchStatement);
        } catch (SQLException e) {
            assert false;
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getKey(GeneralStatus status) {
        Integer id = 0;
        try {
            fetchIdPrimeryKey.setString(1, status.getState().toString());
            fetchIdPrimeryKey.setInt(2, status.getAppId());
            fetchIdPrimeryKey.setString(3, status.getDate());
            fetchIdPrimeryKey.setString(4, status.getPrivateDescriptions());
            fetchIdPrimeryKey.setString(5, status.getPublicDescriptions());
            ResultSet resultSet = fetchIdPrimeryKey.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
            }
            return id;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
