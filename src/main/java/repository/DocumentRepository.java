package main.java.repository;

import main.java.model.Document;
import main.java.model.Type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shayan
 */
public final class DocumentRepository extends RepositoryBase implements IRepository<Document>, SearchRepository<Document> {

    public DocumentRepository(Connection connection) {
        super(connection);
        prepareStatements();
    }

    private PreparedStatement getByAppID;

    public List<Document> selectByAppID(Integer id) {
        try {
            getByAppID.setInt(1, id);
            return fetchData(getByAppID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assert false;//fixme
        return null;
    }

    @Override
    protected void prepareStatements() {
        try {
            selectAllStatement = connection.prepareStatement(
                    "SELECT * FROM " +
                            "hire.document");
            removeStatement = connection.prepareStatement(
                    "DELETE FROM " +
                            "hire.document WHERE id = ?");

            updateStatement = connection.prepareStatement(
                    "UPDATE hire.document" +
                            " SET url=? " +
                            ", doc_type =? WHERE id=? ");

            insertStatement = connection.prepareStatement("INSERT INTO " +
                    "hire.document VALUES (null ,?,?,?)");

            searchStatement = connection.prepareStatement(
                    "SELECT * FROM hire.documentn " +
                            "WHERE doc_type like ? " +
                            "and app_ID = ? ");

            selectRowStatement = connection.prepareStatement(
                    " SELECT * FROM hire.document " +
                            "WHERE ID=?");
            getByAppID = connection.prepareStatement("SELECT * FROM hire" +
                    ".document where app_id = ?");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Document> search(String... str) {
        String[] args = new String[2];

        if (str.length <= 1) {
            args[1] = "any value";
        } else {
            args[1] = str[1];
        }
        if (str.length > 0) {
            args[0] = "%" + str[0];
        }
        args[0] += "%";
        try {
            searchStatement.setString(1, args[0]);
            searchStatement.setString(2, args[1]);
            return fetchData(searchStatement);
        } catch (SQLException e) {
            assert false;
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Document> selectAll() {
        return fetchData(selectAllStatement);
    }

    @Override
    public int insert(Document document) {
        try {
            insertStatement.setString(1, document.getAddress());
            insertStatement.setString(2, document.getType().toString());
            insertStatement.setInt(3, document.getAppID());
            return insertStatement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public int delete(Integer id) {
        try {
            removeStatement.setInt(1, id);
            return removeStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Document selectRow(int id) {

        try {
            selectRowStatement.setInt(1, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return fetchData(selectRowStatement).size() > 0 ? fetchData(selectRowStatement).get(0) : null;
    }

    @Override
    public int update(Document document) {
        try {
            updateStatement.setString(1, document.getAddress());
            updateStatement.setString(2, document.getType().toString());
            updateStatement.setInt(3, document.getID());
            return updateStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public List<Document> fetchData(PreparedStatement preparedStatement) {
        final List<Document> documents = new ArrayList<>();
        try {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Document document =
                        new Document(resultSet.getInt(1),
                                resultSet.getString(2),
                                Type.getValue(resultSet.getString(3)),
                                resultSet.getInt(4));
                documents.add(document);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return documents;
    }
}
