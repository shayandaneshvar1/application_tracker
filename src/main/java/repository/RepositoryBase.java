package main.java.repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Scanner;

public abstract class RepositoryBase {
    protected Connection connection;
    protected PreparedStatement selectAllStatement;
    protected PreparedStatement removeStatement;
    protected PreparedStatement updateStatement;
    protected PreparedStatement searchStatement;
    protected PreparedStatement insertStatement;
    protected PreparedStatement selectRowStatement;

    protected abstract void prepareStatements();

    public RepositoryBase(Connection connection) {
        this.connection = connection;
    }

    public static String[] readConfigs() throws FileNotFoundException {
        String[] result = new String[3];
        Scanner scanner = new Scanner(new File("src/main/resources/config" +
                ".cfg"));
        String[] raw = scanner.nextLine().split("~~");
        for (int i = 0; i < 3; i++) {
            result[i] = raw[i].trim();
        }
        scanner.close();
        return result;
    }
}
