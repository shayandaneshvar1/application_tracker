package main.java.repository;

public interface GetPrimaryKey<T> {

    public Integer getKey(T t);
}
