package main.java.repository;

/**
 * @author k.karimi
 */

import main.java.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class InformationRepository extends RepositoryBase implements IRepository<Information>,
        SearchRepository<Information>, GetPrimaryKey<Information> {

    private PreparedStatement fetchIdPrimeryKey;
    private PreparedStatement specificSearch;

    public InformationRepository(Connection connection) {
        super(connection);
        prepareStatements();
    }

    @Override
    protected void prepareStatements() {
        try {
            selectAllStatement = connection.prepareStatement(
                    "SELECT * FROM " +
                            "information");
            removeStatement = connection.prepareStatement(
                    "DELETE FROM " +
                            "information WHERE id = ?");

            updateStatement = connection.prepareStatement(
                    "UPDATE information" +
                            " SET modified=? " +
                            ", salary=? , address=? , birth_date=?  " +
                            ", description=? , email=? " +
                            ", ID_number = ? , first_name = ?" +
                            ", last_name = ? , mobile = ? , telephone = ?  " +
                            ", degree = ?    , gender = ? , conscription_status =?  " +
                            ", marital_status =? " +
                            "WHERE id=? ");

            insertStatement = connection.prepareStatement("INSERT INTO " +
                    "information VALUES (null ,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            searchStatement = connection.prepareStatement(
                    "SELECT * FROM hire.information " +
                            "WHERE first_name like ? " +
                            "or last_name like ? " +
                            "or email like ? ");

            selectRowStatement = connection.prepareStatement(
                    " SELECT * FROM hire.information " +
                            "WHERE ID=?");
            specificSearch = connection.prepareStatement("SELECT * FROM hire" +
                    ".information WHERE first_name like ? and last_name like " +
                    "?");

            fetchIdPrimeryKey = connection.prepareStatement("SELECT hire.information.ID FROM hire.information " +
                    "WHERE  hire.information.modified = ? " +
                    "AND hire.information.salary = ? AND hire.information.address = ? " +
                    "AND hire.information.birth_date = ? AND hire.information.description = ? " +
                    "AND  hire.information.email = ? AND hire.information.ID_number = ? " +
                    "AND hire.information.first_name= ? AND hire.information.last_name =?  " +
                    "AND hire.information.mobile = ? AND hire.information.telephone = ? " +
                    "AND hire.information.degree = ? AND hire.information.gender = ? " +
                    "AND hire.information.conscription_status =? AND hire.information.marital_status = ?");


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Information> search(String... str) {
        String[] args = new String[3];
        if (str.length > 2) {
            args[2] = "%" + str[2];
        }
        if (str.length > 1) {
            args[1] = "%" + str[1];
        }
        if (str.length > 0) {
            args[0] = "%" + str[0];
        }
        Arrays.stream(args).forEach(x -> {
            x += "%";
            System.out.println(x);
        });
        try {
            if (str.length == 2) {
                specificSearch.setString(1, args[0]);
                specificSearch.setString(2, args[1]);
                return fetchData(specificSearch);
            }
            searchStatement.setString(1, args[0]);
            searchStatement.setString(2, args[1]);
            searchStatement.setString(3, args[2]);
            return fetchData(searchStatement);
        } catch (SQLException e) {
            assert false;
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Information> selectAll() {

        return fetchData(selectAllStatement);
    }

    @Override
    public int insert(Information information) {
        try {
            insertStatement.setString(1, information.getDate());
            insertStatement.setInt(2, information.getReqSalary());
            insertStatement.setString(3, information.getAddress());
            insertStatement.setString(4, information.getBirthDate());
            insertStatement.setString(5, information.getDescription());
            insertStatement.setString(6, information.getEmail());
            insertStatement.setString(7, information.getIDNumber());
            insertStatement.setString(8, information.getFirstName());
            insertStatement.setString(9, information.getLastName());
            insertStatement.setString(10, information.getMobile());
            insertStatement.setString(11, information.getTelephone());
            insertStatement.setString(12, information.getDegree().toString());
            insertStatement.setString(13, information.getGender().toString());
            insertStatement.setString(14, information.getConscriptionStatus().toString());
            insertStatement.setString(15, information.getMaritalStatus().toString());
            return insertStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

    @Override
    public int delete(Integer id) {
        PreparedStatement tempStatment = removeStatement;
        try {
            removeStatement.setInt(1, id);
            return tempStatment.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
    }

    @Override
    public Information selectRow(int id) {

        try {
            selectRowStatement.setInt(1, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return fetchData(selectRowStatement).size() > 0 ? fetchData(selectRowStatement).get(0) : null;
    }

    @Override
    public int update(Information information) {
        try {
            updateStatement.setString(1, information.getDate());
            updateStatement.setInt(2, information.getReqSalary());
            updateStatement.setString(3, information.getAddress());
            updateStatement.setString(4, information.getBirthDate());
            updateStatement.setString(5, information.getDescription());
            updateStatement.setString(6, information.getEmail());
            updateStatement.setString(7, information.getIDNumber());
            updateStatement.setString(8, information.getFirstName());
            updateStatement.setString(9, information.getLastName());
            updateStatement.setString(10, information.getMobile());
            updateStatement.setString(11, information.getTelephone());
            updateStatement.setString(12, information.getDegree().toString());
            updateStatement.setString(13, information.getGender().toString());
            updateStatement.setString(14, information.getConscriptionStatus().toString());
            updateStatement.setString(15, information.getMaritalStatus().toString());
            updateStatement.setInt(16, information.getID());
            return updateStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
//            throw new RuntimeException(e);
            return 0;
        }
    }

    @Override
    public List<Information> fetchData(PreparedStatement preparedStatement) {
        final List<Information> info = new ArrayList<>();
        try {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Information information =
                        new Information(resultSet.getInt(1),
                                Event.formatDate(resultSet.getString(2)),
                                resultSet.getInt(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getString(6),
                                resultSet.getString(7),
                                resultSet.getString(8),
                                resultSet.getString(9),
                                resultSet.getString(10),
                                resultSet.getString(11),
                                resultSet.getString(12),
                                Degree.getValue(resultSet.getString(13)),
                                Gender.getValue(resultSet.getString(14)),
                                ConscriptionStatus.getValue(resultSet.getString(15)),
                                MaritalStatus.getValue(resultSet.getString(16)));
                info.add(information);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return info;
    }


    @Override
    public Integer getKey(Information information) {
        Integer id = 0;
        try {
            fetchIdPrimeryKey.setString(1, information.getDate());
            fetchIdPrimeryKey.setInt(2, information.getReqSalary());
            fetchIdPrimeryKey.setString(3, information.getAddress());
            fetchIdPrimeryKey.setString(4, information.getBirthDate());
            fetchIdPrimeryKey.setString(5, information.getDescription());
            fetchIdPrimeryKey.setString(6, information.getEmail());
            fetchIdPrimeryKey.setString(7, information.getIDNumber());
            fetchIdPrimeryKey.setString(8, information.getFirstName());
            fetchIdPrimeryKey.setString(9, information.getLastName());
            fetchIdPrimeryKey.setString(10, information.getMobile());
            fetchIdPrimeryKey.setString(11, information.getTelephone());
            fetchIdPrimeryKey.setString(12, information.getDegree().toString());
            fetchIdPrimeryKey.setString(13, information.getGender().toString());
            fetchIdPrimeryKey.setString(14, information.getConscriptionStatus().toString());
            fetchIdPrimeryKey.setString(15, information.getMaritalStatus().toString());

            ResultSet resultSet = fetchIdPrimeryKey.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
            }
            return id;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
