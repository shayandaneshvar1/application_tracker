package main.java.repository;

import java.util.List;
@FunctionalInterface
public interface SearchRepository<T> {
    public List<T> search(String... similarity);
}
