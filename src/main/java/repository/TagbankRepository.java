package main.java.repository;

import main.java.model.Application;
import main.java.model.Interview;
import main.java.model.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class TagbankRepository extends RepositoryBase {
    private PreparedStatement fetchTagByTagId;
    public TagbankRepository(Connection connection) {
        super(connection);
        prepareStatements();
    }

    @Override
    protected void prepareStatements() {

        try {
            insertStatement = connection.prepareStatement("INSERT INTO hire.tagBank VALUES(?,?)");
            removeStatement = connection.prepareStatement("DELETE FROM hire.tagBank WHERE tag_ID = ? or applicationID=?");
            updateStatement = connection.prepareStatement("UPDATE hire.tagBank SET tag_ID = ? or applicationID=? ");
            fetchTagByTagId = connection.prepareStatement("select hire.tag.title from hire.tag " +
                    "join hire.tagbank on hire.tagBank.applicationID = hire.application.ID " +
                    "where hire.application.ID=?");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int insert(Tag idTag, Application idApp){
        try {
            insertStatement.setInt(1,idTag.getID());
            insertStatement.setInt(2,idApp.getID());
            return insertStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int delete (Tag idTag, Application idApp){
        try {
            removeStatement.setInt(1,idTag.getID());
            removeStatement.setInt(2,idApp.getID());
            return removeStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public int updateTwo (Tag idTag, Application idApp){
        try {
            updateStatement.setInt(1,idTag.getID());
            updateStatement.setInt(2,idApp.getID());
            return updateStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
