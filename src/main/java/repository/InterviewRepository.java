package main.java.repository;

import main.java.model.Application;
import main.java.model.Interview;
import main.java.model.InterviewStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class InterviewRepository extends RepositoryBase implements SearchRepository<Interview> {

    protected PreparedStatement searchInterviewByAppId;

    public InterviewRepository(Connection connection) {
        super(connection);

        prepareStatements();
    }

    @Override
    protected void prepareStatements() {

        try {
            selectAllStatement = connection.prepareStatement("SELECT * FROM interview"
            );

            removeStatement = connection.prepareStatement("DELETE FROM " +
                    "interview WHERE id = ?");

            updateStatement = connection.prepareStatement("update interview set date_interview = ?," +
                    "interviewer = ?, subject_interview = ?," +
                    " status_interview = ? ," +
                    " point_result_interview = ? where id = ?");

            insertStatement = connection.prepareStatement("INSERT into " +
                    "interview values (null ,?, ? , ? , ? , ? , ? )");

            searchStatement = connection.prepareStatement("SELECT * FROM " +
                    "interview where  interviewer like ? or status_interview like ? " +
                    "or subject_interview like ? or date_interview like ? or point_result_interview like ? or " +
                    "app_id like ?");

            selectRowStatement = connection.prepareStatement("select * from  interview where id = ?"
            );
            searchInterviewByAppId = connection.prepareStatement("select * " +
                    "from  interview where hire.interview.app_id = ? ");


        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

    }

    //tested suc
    @Override
    public List<Interview> search(String... str) {

        String[] args = new String[6];
        if (str.length > 5) {
            args[5] = "%" + str[5];
        }
        if (str.length > 4) {
            args[4] = "%" + str[4];
        }
        if (str.length > 3) {
            args[3] = "%" + str[3];
        }
        if (str.length > 2) {
            args[2] = "%" + str[2];
        }
        if (str.length > 1) {
            args[1] = "%" + str[1];
        }
        if (str.length > 0) {
            args[0] = "%" + str[0];
        }
        Arrays.stream(args).forEach(x -> x += "%");
        try {
            searchStatement.setString(1, args[0]);
            searchStatement.setString(2, args[1]);
            searchStatement.setString(3, args[2]);
            searchStatement.setString(4, args[3]);
            searchStatement.setString(5, args[4]);
            searchStatement.setString(6, args[5]);
            return fetchData(searchStatement);
        } catch (SQLException e) {
            assert false;
            e.printStackTrace();
        }

        return null;
    }


    // tested suc
    public List<Interview> selectAll() {
        return fetchData(selectAllStatement);
    }

    // teseted suc
    public int insert(Interview interview, Application application) {

        try {
//            insertStatement.setInt(1,interview.getID());
            insertStatement.setString(1, interview.getDate());
            insertStatement.setString(2, interview.getInterViewer());
            insertStatement.setString(3, interview.getSubject());
            insertStatement.setString(4, interview.getInterviewStatus().toString());
            insertStatement.setString(5, interview.getPointResultInterview());
            insertStatement.setInt(6, application.getID());

            return insertStatement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    // teseted suc
    public int delete(Integer idInterview) {
        try {
            removeStatement.setInt(1, idInterview);
            return removeStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    // tested suc
    public Interview selectRow(int id) {

        try {
            selectRowStatement.setInt(1, id);
            return fetchData(selectRowStatement).get(0);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

    }

    // tested suc
    public List<Interview> searchInterviewByAppId(int id) {
        try {
            searchInterviewByAppId.setInt(1, id);
            return fetchData(searchInterviewByAppId);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

    }

    //tested suc
    public int update(Interview interview, Application application) {

        try {
            updateStatement.setString(1, interview.getDate());
            updateStatement.setString(2, interview.getInterViewer());
            updateStatement.setString(3, interview.getSubject());
            updateStatement.setString(4, interview.getInterviewStatus().toString());
            updateStatement.setString(5, interview.getPointResultInterview());
            updateStatement.setInt(6, interview.getID());

            return updateStatement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    // tested suc
    public List<Interview> fetchData(PreparedStatement preparedStatement) {
        //fix this func  = fixed in 8/14
        final List<Interview> interviews = new ArrayList<>();
        try (ResultSet resultSet = preparedStatement.executeQuery();) {
            while (resultSet.next()) {
                Interview interview = new Interview(
                        resultSet.getInt(1),
                        Interview.formatDate(resultSet.getString(2)),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        InterviewStatus.getValue(resultSet.getString(5)),
                        resultSet.getString(6)
                );
                interviews.add(interview);

            }
            return interviews;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
