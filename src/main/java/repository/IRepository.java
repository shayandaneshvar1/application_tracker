package main.java.repository;

import java.sql.PreparedStatement;
import java.util.List;

/**
 * @author mahshid
 */
public interface IRepository<T> {

    public List<T> selectAll();

    public int insert(T t);

    public int delete(Integer t);

    public T selectRow(int id);

    public int update(T t);

    public List<T> fetchData(PreparedStatement preparedStatement);

}
