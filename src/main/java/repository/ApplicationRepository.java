package main.java.repository;

/**
 * @author k.karimi
 */


import main.java.model.Application;
import main.java.model.Document;
import main.java.model.Event;
import main.java.model.GeneralStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public final class ApplicationRepository extends RepositoryBase implements IRepository<Application> {

    private PreparedStatement fetchApplicationsByTagId;
    private PreparedStatement getByInfoID;

    public ApplicationRepository(Connection connection) {
        super(connection);
        prepareStatements();
    }

    @Override
    protected void prepareStatements() {
        try {
            selectAllStatement = connection.prepareStatement("SELECT * FROM " +
                    "application");
            removeStatement = connection.prepareStatement("DELETE FROM " +
                    "application WHERE id = ?");
            updateStatement = connection.prepareStatement("UPDATE application" +
                    "  SET info_ID=? , creation_date =? WHERE id=?");
            insertStatement = connection.prepareStatement("INSERT into " +
                    "application values (null ,?,?)");
            searchStatement = connection.prepareStatement("SELECT * FROM " +
                    "application where id = ? and info_ID = ? and " +
                    "creation_date = ?");
            selectRowStatement = connection.prepareStatement("SELECT * FROM hire.application WHERE ID =?");
            fetchApplicationsByTagId = connection.prepareStatement("select hire.application.ID,hire.application.info_ID ,hire.application.creation_date from hire.application " +
                    "join tagBank on hire.tagBank.applicationID = hire.application.ID " +
                    "where tag_ID=?");
            getByInfoID = connection.prepareStatement("SELECT * from hire" +
                    ".application where info_ID = ?");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Application> getByInfoID(Integer id) {
        try {
            getByInfoID.setInt(1, id);
            return fetchData(getByInfoID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Application> selectAll() {
        return fetchData(selectAllStatement);
    }

    public List<Application> selectLatest() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM hire.application order by ID desc limit 10");
            return fetchData(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Application> fetchData(PreparedStatement statement) {
        // FIXME: 8/18/2019
        final List<Application> list = new ArrayList<>();
        // FIXME: 8/18/2019
        try {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer infoID = resultSet.getInt(2);
                Integer appID = resultSet.getInt(1);
                Application application = new Application(appID,
                        RepositoryManager.getInstance().getInformationRepo().
                                selectRow(infoID), resultSet.getString(3),
                        new HashSet<Document>(RepositoryManager.getInstance().
                                getDocumentRepository().selectByAppID(appID)),
                        new HashSet<GeneralStatus>(RepositoryManager.getInstance
                                ().getStatusRepository().selectByAppID(appID)),
                        new HashSet<>(RepositoryManager.getInstance().getInterviewRepo().searchInterviewByAppId(appID)),
                        new HashSet<>(RepositoryManager.getInstance().getTagRepository().fetchTagsByApplicationID(appID)));
                list.add(application);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int insert(Application application) {
        try {
            insertStatement.setInt(1, application.getInformation().getID());
            insertStatement.setString(2, Event.DateToString(application.getCreationDate()));
            return insertStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delete(Integer id) {

        try {
            removeStatement.setInt(1, id);
            return removeStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Application selectRow(int id) {
        try {
            selectRowStatement.setInt(1, id);
            return fetchData(selectRowStatement).size() > 0 ? fetchData(selectRowStatement).get(0) : null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int update(Application application) {
        return 0;
    }

    public List<Application> fetchApplicationsByTagID(Integer tagID) {
        try {
            fetchApplicationsByTagId.setInt(1, tagID);
            return fetchData(fetchApplicationsByTagId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
