package main.java.repository;

import main.java.model.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TagRepository extends RepositoryBase implements IRepository<Tag>
        , GetPrimaryKey<Tag> {
    private PreparedStatement getPrimaryKey;
    private PreparedStatement searchByAppIDStatement;

    public TagRepository(Connection connection) {
        super(connection);
        prepareStatements();
    }

    @Override
    public List<Tag> selectAll() {
        return fetchData(selectAllStatement);
    }

    @Override
    public int insert(Tag tag) {
        try {
            insertStatement.setString(1, tag.getTitle());
            return insertStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int delete(Integer t) {
        PreparedStatement tempStatment = removeStatement;
        try {
            removeStatement.setInt(1, t);
            return tempStatment.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Tag selectRow(int id) {
        // List<Tag> list = new ArrayList<>();
        try {
            ResultSet resultSet = selectRowStatement.executeQuery();

            //while (resultSet.next()) {
            Tag tag = new Tag(resultSet.getInt(1),
                    resultSet.getString(2));
            // list.add(tag);
            // }
            return tag;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public int update(Tag tag) {
        PreparedStatement tempStatement = updateStatement;
        try {
            tempStatement.setInt(2, tag.getID());
            tempStatement.setString(1, tag.getTitle());
            return tempStatement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

    }

    @Override
    public List<Tag> fetchData(PreparedStatement preparedStatement) {
        final List<Tag> list = new ArrayList<>();
        try {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag(
                        resultSet.getInt(1),
                        resultSet.getString(2)

                );
                list.add(tag);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


    @Override
    protected void prepareStatements() {
        try {
            selectAllStatement = connection.prepareStatement("SELECT * FROM " +
                    "hire.tag");
            selectRowStatement = connection.prepareStatement(
                    " SELECT * FROM tag " + "WHERE ID=?");
            removeStatement = connection.prepareStatement("DELETE FROM "
                    + "tag WHERE id = ?");
            updateStatement = connection.prepareStatement("UPDATE tag" +
                    "  SET title=?" +
                    "WHERE id=?");
            insertStatement = connection.prepareStatement("INSERT into tag values ( null ,?)");
            searchStatement = connection.prepareStatement("SELECT * FROM " +
                    "tag where id = ? and title = ?");
            getPrimaryKey = connection.prepareStatement("SELECT hire.tag.ID " +
                    "FROM hire.tag " +
                    "WHERE  hire.tag.title = ? ");
            searchByAppIDStatement = connection.prepareStatement("select hire" +
                    ".tag.id,hire.tag.title from hire.tag join tagBank on " +
                    "hire.tagBank.tag_ID = hire.tag.ID where applicationID= ?");
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Integer getKey(Tag tag) {
        Integer id = 0;
        try {
            getPrimaryKey.setString(1, tag.getTitle());
            ResultSet resultSet = getPrimaryKey.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
            }
            return id;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public List<Tag> fetchTagsByApplicationID(Integer appID){
        try {
            searchByAppIDStatement.setInt(1,appID);
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            fetchData(searchByAppIDStatement).forEach(System.out::println);
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            return fetchData(searchByAppIDStatement);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
