package main.java.repository;

import main.java.model.Application;
import main.java.model.GeneralStatus;
import main.java.model.Information;
import main.java.model.State;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Shayan
 */
//Singleton
public final class RepositoryManager {
    private static RepositoryManager ourInstance = new RepositoryManager();

    private ApplicationRepository applicationRepo;
    private InformationRepository informationRepo;
    private InterviewRepository interviewRepo;
    private GeneralStatusRepository statusRepository;
    private TagbankRepository tagbankDAO;
    private TagRepository tagRepository;
    private DocumentRepository documentRepository;

    public DocumentRepository getDocumentRepository() {
        return documentRepository;
    }

    public static RepositoryManager getOurInstance() {
        return ourInstance;
    }

    public State getLatestState(Integer id) {
        List<GeneralStatus> list = statusRepository.selectByAppID(id);
        Collections.reverse(list);
        return list.get(0).getState();
    }

    public GeneralStatusRepository getStatusRepository() {
        return statusRepository;
    }

    public ApplicationRepository getApplicationRepo() {
        return applicationRepo;
    }

    public InformationRepository getInformationRepo() {
        return informationRepo;
    }

    public InterviewRepository getInterviewRepo() {
        return interviewRepo;
    }

    public TagbankRepository getTagbankDAO() {
        return tagbankDAO;
    }

    public TagRepository getTagRepository() {
        return tagRepository;
    }

    public static RepositoryManager getInstance() {
        return ourInstance;
    }

    public List<Application> getLatestApplications() {
        return applicationRepo.selectLatest();
    }

    public List<Application> generalSearch(String name, String lastname,
                                           String email) {
        List<Information> information;
        if (!email.equalsIgnoreCase(name)) {
            information = getInformationRepo().search(name,
                    lastname);
            System.out.println("X");
        } else {
            information = getInformationRepo().search(name,
                    lastname, email);
        }

        List<Application> applications = new ArrayList<>();
        information.forEach(x -> {
            List<Application> temp = null;
            if ((temp = getApplicationRepo().getByInfoID(x.getID())).size() > 0) {
                applications.add(temp.get(0));
            }
        });
        return applications;
    }

    public List<Application> generalSearch(String... strings) {
        if (strings.length > 2) {
            return generalSearch(strings[0], strings[strings.length - 1],
                    strings[1]);
        } else if (strings.length > 1) {
            return generalSearch(strings[0], strings[1], strings[1]);
        }
        return generalSearch(strings[0], strings[0], strings[0]);
    }

    private RepositoryManager() {
        try {
            String[] configs = RepositoryBase.readConfigs();
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:" + configs[2] + "/hire",
                    configs[1], configs[0]);
            applicationRepo = new ApplicationRepository(connection);
            informationRepo = new InformationRepository(connection);
            interviewRepo = new InterviewRepository(connection);
            tagbankDAO = new TagbankRepository(connection);
            tagRepository = new TagRepository(connection);
            statusRepository = new GeneralStatusRepository(connection);
            documentRepository = new DocumentRepository(connection);
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
