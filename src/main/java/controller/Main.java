package main.java.controller;

import main.java.view.View;

/**
 * @author Shayan
 */
public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller(new View());
    }
}
