package main.java.controller;

import main.java.view.View;

/**
 * @author Shayan
 */
public final class Controller {

    private static View view;

    public Controller(View view) {
        this.view = view;
        view.launcher();
    }

}
