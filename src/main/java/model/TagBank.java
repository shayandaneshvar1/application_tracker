package main.java.repository;

import main.java.model.Application;
import main.java.model.Tag;

public  class TagBank {

    private final Integer id;
    private Tag idTag;
    private Application idApp;

    public TagBank(Integer id, Tag idTag, Application idApp) {
        this.id = id;
        this.idTag = idTag;
        this.idApp = idApp;
    }

    public Integer getId() {
        return id;
    }

    public Tag getIdTag() {
        return idTag;
    }

    public Application getIdApp() {
        return idApp;
    }

    @Override
    public String toString() {
        return "TagBank{" +
                "id=" + id +
                ", idTag=" + idTag +
                ", idApp=" + idApp +
                '}';
    }
}
