package main.java.model;

import java.util.Objects;

public class Tag {
    private  Integer ID;
    private String title;

    public Tag(Integer ID, String title) {
        this.ID = ID;
        setTitle(title);
    }

    private void setTitle(String title) {
        this.title = title;
    }

    public Integer getID() {
        return ID;
    }

    public String getTitle() {
        return title;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return Objects.equals(ID, tag.ID) &&
                Objects.equals(title, tag.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, title);
    }

    @Override
    public String toString() {
        return "Tag{" +
                "ID=" + ID +
                ", title='" + title + '\'' +
                '}';
    }
}
