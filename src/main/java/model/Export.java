package main.java.model;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * @author shahab
 */
public final class Export {
    private Export() {
    }

    public static void CreateZip(Application application, String dir) {
        String pdfDir = dir + "/" + application.getID() + "_" + application.getInformation().getFullname()+".pdf";
        CreatePDF createPDF =
                new CreatePDF(pdfDir, application);
        String zipFile = dir + "/" + application.getID() + "_" + application.getInformation().getFullname() + ".zip";
        Set<Document> documents = application.getDocuments();
        ArrayList<String> srcFiles = new ArrayList<>();
        srcFiles.add(pdfDir);
        Document document = null;
        while (documents.iterator().hasNext()) {
            document = documents.iterator().next();
            srcFiles.add(document.getAddress());
        }

        try {

            // create byte buffer

            byte[] buffer = new byte[1024];

            FileOutputStream fos = new FileOutputStream(zipFile);

            ZipOutputStream zos = new ZipOutputStream(fos);

            for (int i = 0; i < srcFiles.size(); i++) {

                File srcFile = new File(srcFiles.get(i));

                FileInputStream fis = new FileInputStream(srcFile);

                // begin writing a new ZIP entry, positions the stream to the
                // start of the entry data

                zos.putNextEntry(new ZipEntry(srcFile.getName()));

                int length;

                while ((length = fis.read(buffer)) > 0) {

                    zos.write(buffer, 0, length);

                }

                zos.closeEntry();

                // close the InputStream

                fis.close();

            }

            // close the ZipOutputStream

            zos.close();
            Files.deleteIfExists(Paths.get(pdfDir));

        } catch (IOException ioe) {

            System.out.println("Error creating zip file: " + ioe);

        }
    }
}
