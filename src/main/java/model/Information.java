package main.java.model;

import java.util.Date;

public class Information extends Event {
    private Integer ID;
    private String address;
    private String birthDate;
    private String email;
    private String IDNumber;
    private String firstName;
    private String lastName;
    private String telephone;
    private String mobile;
    private ConscriptionStatus conscriptionStatus;
    private Degree degree;
    private Gender gender;
    private MaritalStatus maritalStatus;
    private String description;

    public void setID(Integer ID) {
//        assert ID == null;
        if (this.ID == null) {
            this.ID = ID;
        }
        System.out.println(ID);
        System.out.println(this.ID);
    }

    private Integer reqSalary;

    public Information(Integer ID, Date date, Integer reqSalary, String address,
                       String birthDate, String description, String email,
                       String IDNumber, String firstName, String lastName,
                       String mobile, String telephone, Degree degree,
                       Gender gender, ConscriptionStatus conscriptionStatus,
                       MaritalStatus maritalStatus) {
        super(date);
        this.ID = ID;
        this.address = address;
        this.birthDate = birthDate;
        this.email = email;
        this.IDNumber = IDNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephone = telephone;
        this.mobile = mobile;
        this.conscriptionStatus = conscriptionStatus;
        this.degree = degree;
        this.gender = gender;
        this.maritalStatus = maritalStatus;
        this.description = description;
        this.reqSalary = reqSalary;
    }

    public Integer getID() {
        return ID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIDNumber() {
        return IDNumber;
    }

    public void setIDNumber(String IDNumber) {
        this.IDNumber = IDNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public ConscriptionStatus getConscriptionStatus() {
        return conscriptionStatus;
    }

    public void setConscriptionStatus(ConscriptionStatus conscriptionStatus) {
        this.conscriptionStatus = conscriptionStatus;
    }

    public Degree getDegree() {
        return degree;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getReqSalary() {
        return reqSalary;
    }

    public void setReqSalary(Integer reqSalary) {
        this.reqSalary = reqSalary;
    }

    public String getFullname() {
        return firstName + " " + lastName;
    }

    @Override
    public String toString() {
        return "Information{" +
                "email='" + email + '\'' +
                ", IDNumber='" + IDNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", telephone='" + telephone + '\'' +
                ", gender=" + gender +
                '}';
    }
}
