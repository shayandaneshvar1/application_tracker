package main.java.model;

public enum MaritalStatus {
    SINGLE, MARRIED, NONE;


    public static MaritalStatus getValue(String string) {
        if (string.equalsIgnoreCase("single")) {
            return MaritalStatus.SINGLE;
        } else if (string.equalsIgnoreCase("married")) {
            return MaritalStatus.MARRIED;
        } else {
            return MaritalStatus.NONE;
        }
    }

}
