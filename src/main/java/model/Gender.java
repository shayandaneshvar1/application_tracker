package main.java.model;

public enum Gender {
    MALE, FEMALE, OTHER, NONE;


    public static Gender getValue(String string) {
        if (string.equalsIgnoreCase("male")) {
            return Gender.MALE;
        } else if (string.equalsIgnoreCase("female")) {
            return Gender.FEMALE;
        } else if (string.equalsIgnoreCase("other")) {
            return Gender.OTHER;
        } else {
            return Gender.NONE;
        }
    }
}
