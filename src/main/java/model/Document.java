package main.java.model;

/**
 * @author Shayan
 */
public class Document {
    private final Integer ID;
    private String address;
    private Type type;
    private final Integer appID;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAppID() {
        return appID;
    }

    public Document(Integer ID, String address, Type type, Integer appID) {
        this.ID = ID;
        this.address = address;
        this.type = type;
        this.appID = appID;
    }

    public Integer getID() {
        return ID;
    }


    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
