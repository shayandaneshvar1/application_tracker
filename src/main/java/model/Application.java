package main.java.model;

/**
 * @author k.karimi
 */

import javax.mail.Message;
import java.io.*;
import java.util.*;

public class Application {
    private final Integer ID;
    private final Information information;
    private Date creationDate;
    private Set<Document> documents;
    private Set<GeneralStatus> statuses;
    private Set<Interview> interviews;
    private Set<Tag> tags;

    //Not used when reading from db
    public Application(Integer ID, Information info) {
        this.ID = ID;
        information = info;
        documents = new HashSet<>();
        statuses = new HashSet<>();
        interviews = new HashSet<>();
        tags = new HashSet<>();
        creationDate = Event.formatDate(Event.DateToString(Calendar.getInstance().getTime()));
    }


    public Application(Integer ID, Information information, Date creationDate
            , Set<Document> docs, Set<GeneralStatus> status,
                       Set<Interview> interviews, Set<Tag> tags) {
        this.ID = ID;
        this.information = information;
        this.creationDate = creationDate;
        documents = docs;
        statuses = status;
        this.interviews = interviews;
        this.tags = tags;
    }

    public Application(Integer ID, Information information, String creationDate,
                       Set<Document> documents, Set<GeneralStatus> statuses,
                       Set<Interview> interviews, Set<Tag> tags) {
        this(ID, information, Event.formatDate(creationDate), documents,
                statuses, interviews, tags);
    }

    public static void sendEmail(Application application,GeneralStatus generalStatus) {
        File File = new File("main/resources/sendEmailText.txt");
        String text = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(File));
            String st;
            while ((st = bufferedReader.readLine()) != null) {
                text += st;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        text = String.format(text, application.information.getFullname(),
                generalStatus.getPublicDescriptions(), "Mapsa");
        SendEmail.send_ssl_mail(application.information.getEmail(), "subject", text
                , Message.RecipientType.TO);
    }

    public Set<Document> getDocuments() {
        return (Set<Document>) ((HashSet<Document>) documents).clone();
    }

    public Information getInformation() {
        return information;
    }

    public Set<GeneralStatus> getStatuses() {
        return (Set<GeneralStatus>) ((HashSet<GeneralStatus>) statuses).clone();
    }

    public Set<Interview> getInterviews() {
        return (Set<Interview>) ((HashSet<Interview>) interviews).clone();
    }

    public Set<Tag> getTags() {
        return (Set<Tag>) ((HashSet<Tag>) tags).clone();
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public GeneralStatus getLatestStatus() {
        GeneralStatus latestStatus = null;
        if (statuses.size() == 0) {
            return latestStatus;
        }
        Iterator<GeneralStatus> statusIterator = statuses.iterator();
        latestStatus = statusIterator.next();
        while (statusIterator.hasNext()) {
            GeneralStatus temp = statusIterator.next();
            if (temp.getID() > latestStatus.getID()) {
                latestStatus = temp;
            }
        }
        return latestStatus;
    }

    public Integer getID() {
        return ID;
    }

    public void addToTags(Tag tag) {
        tags.add(tag);
    }

    public void addToInterviews(Interview interview) {
        interviews.add(interview);
    }

    public void addToStatuses(GeneralStatus status) {
        statuses.add(status);
    }

    public void renewStatus(GeneralStatus status) {
        statuses.add(status);
    }

    public void updateStatus(GeneralStatus status) {
        statuses.add(status);
    }

    public void addToDocs(Document document) {
        documents.add(document);
    }

    public boolean removeDoc(Document document) {
        return documents.remove(document);
    }

    public boolean removeInterview(Interview interview) {
        return interviews.remove(interview);
    }

    public boolean removeTag(Tag tag) {
        Iterator<Tag> iterator = tags.iterator();
        while (iterator.hasNext()) {
            Tag temp = iterator.next();
            if (temp.getID() == tag.getID()) {
                return tags.remove(temp);
            }
        }
        return false;
    }


}
