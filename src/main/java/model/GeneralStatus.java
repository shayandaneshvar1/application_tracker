package main.java.model;

import java.util.Date;

/**
 * @author Modaberi
 */
public class GeneralStatus extends Event {
    private  Integer ID;
    private int appId;
    private State state;
    private String publicDescriptions;
    private String privateDescriptions;

    public GeneralStatus( Integer ID,State state, Integer appId,Date date
            , String privateDescriptions, String publicDescriptions) {
        super(date);
        this.ID = ID;
        this.appId = appId;

        this.publicDescriptions = publicDescriptions;
        this.state = state;
        this.privateDescriptions = privateDescriptions;
    }

    public Integer getID() {
        return ID;
    }

    public int getAppId() {
        return appId;
    }


    public String getPublicDescriptions() {
        return publicDescriptions;
    }

    public State getState() {
        return state;
    }

    public String getPrivateDescriptions() {
        return privateDescriptions;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setPublicDescriptions(String publicDescriptions) {
        this.publicDescriptions = publicDescriptions;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setPrivateDescriptions(String privateDescriptions) {
        this.privateDescriptions = privateDescriptions;
    }
}
