package main.java.model;

public enum Type {
    CV,ID,IMAGE,OTHER,NONE;

    public static Type getValue(String string) {
        if (string.equalsIgnoreCase("cv")) {
            return Type.CV;
        } else if (string.equalsIgnoreCase("id")) {
            return Type.ID;
        } else if (string.equalsIgnoreCase("image")) {
            return Type.IMAGE;
        } else if (string.equalsIgnoreCase("other")) {
            return Type.OTHER;
        } else {
            return Type.NONE;
        }
    }
}
