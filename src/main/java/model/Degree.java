package main.java.model;

public enum Degree {
    DIPLOMA, ASSOCIATES, BACHELORS, MASTERS, PhD, NONE;


    public static Degree getValue(String string) {
        if (string.equalsIgnoreCase("diploma")) {
            return Degree.DIPLOMA;
        } else if (string.equalsIgnoreCase("phd")) {
            return Degree.PhD;
        } else if (string.equalsIgnoreCase("bachelors")) {
            return Degree.BACHELORS;
        } else if (string.equalsIgnoreCase("masters")) {
            return Degree.MASTERS;
        } else if (string.equalsIgnoreCase("associates")) {
            return Degree.ASSOCIATES;
        } else {
            return Degree.NONE;
        }
    }
}
