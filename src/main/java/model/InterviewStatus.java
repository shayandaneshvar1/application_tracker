package main.java.model;

/*
Intended To Be Used in Interview Class
 */
public enum InterviewStatus {
    NOT_ATTENDED, ATTENDED, MISSED, NONE;

    public static InterviewStatus getValue(String string) {
        if (string.equalsIgnoreCase("attended")) {
            return InterviewStatus.ATTENDED;
        } else if (string.equalsIgnoreCase("not_attended")) {
            return InterviewStatus.NOT_ATTENDED;
        } else if (string.equalsIgnoreCase("missed")) {
            return InterviewStatus.MISSED;
        } else {
            return InterviewStatus.NONE;
        }
    }
}
