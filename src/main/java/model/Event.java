package main.java.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Event {
    private Date date;

    public Event(Date date) {
        this.date = date;
    }

    public String getDate() {
        return date.getYear() + "," + date.getMonth() + "," + date.getDay() +
                "," + date.getHours() + "," + date.getMinutes();
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public static Date formatDate(String rawDate) {
        String[] date = rawDate.split(",");
        return new Date(
                Integer.valueOf(String.valueOf((date.length > 1) ? date[0] : 0)),
                Integer.valueOf(String.valueOf((date.length > 2) ? date[1] : 0)),
                Integer.valueOf(String.valueOf((date.length > 3) ? date[2] : 0)),
                Integer.valueOf(String.valueOf((date.length > 4) ? date[3] : 0)),
                Integer.valueOf(String.valueOf((date.length > 5) ? date[4] : 0)),
                Integer.valueOf(String.valueOf((date.length > 6) ? date[5] : 0))
        );
    }

    public static String DateToString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy,MM,dd,hh,mm");
        return format.format(date);
    }
}
