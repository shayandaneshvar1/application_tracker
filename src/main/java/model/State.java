package main.java.model;

/**
 * Intended To Be Used In General InterviewStatus Class
 */
public enum State {
    ADDED, INTERVIEW, PASSED, FAILED, NONE;

    public static State getValue(String string) {
        if (string.equalsIgnoreCase("ADDED")) {
            return State.ADDED;
        } else if (string.equalsIgnoreCase("INTERVIEW")) {
            return State.INTERVIEW;
        } else if (string.equalsIgnoreCase("PASSED")) {
            return State.PASSED;
        } else if (string.equalsIgnoreCase("FAILED")) {
            return State.FAILED;
        } else {
            return State.NONE;
        }

    }
}
