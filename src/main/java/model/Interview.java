package main.java.model;

import java.util.Date;

/**
 * @author Brehon
 */

public class Interview extends Event {

    // == field (attribute) == \\
    private final Integer ID;
    private  String interViewer;
    private final String subject;
    private  String pointResultInterview;
    private  InterviewStatus interviewStatus;


    // == constructor == \\

    public Interview(Integer ID,Date date ,String interViewer, String subect,
                     InterviewStatus interviewStatus) {
        super(date);
        this.ID = ID;
        this.interViewer = interViewer;
        this.subject = subect;
        this.pointResultInterview = "";
        this.interviewStatus = interviewStatus;
    }

    // for dao DB
    public Interview( Integer ID,  Date date,  String interViewer, String subject, InterviewStatus interviewStatus,
                      String pointResultInterview) {
        super(date);
        this.ID = ID;
        this.interViewer = interViewer;
        this.subject = subject;
        this.pointResultInterview = pointResultInterview;
        this.interviewStatus = interviewStatus;
    }

    // == Getter == \\
    public String getInterViewer() {
        return interViewer;
    }




    public Integer getID() {
        return ID;
    }

    public String getSubject() {
        return subject;
    }

    public InterviewStatus getInterviewStatus() {
        return interviewStatus;
    }

    // == setter == \\


    public String getPointResultInterview() {
        return pointResultInterview;
    }

    public void setPointResultInterview(String pointResultInterview) {
        this.pointResultInterview = pointResultInterview;
    }

    public void setInterViewer(String interViewer) {
        this.interViewer = interViewer;
    }


    public void setInterviewStatus(InterviewStatus interviewStatus) {

        this.interviewStatus = interviewStatus;
    }

    @Override
    public String toString() {
        return "Interview{" +
                "ID=" + ID +
                ", interViewer='" + interViewer + '\'' +
                ", subject='" + subject + '\'' +
                ", pointResultInterview=" + pointResultInterview +
                ", interviewStatus=" + interviewStatus +
                '}';
    }
}
