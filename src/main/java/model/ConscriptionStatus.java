package main.java.model;

public enum ConscriptionStatus {
    EXEMPT, PENDING, DONE, SERVING, NONE;


    public static ConscriptionStatus getValue(String string) {
        if (string.equalsIgnoreCase("exempt")) {
            return ConscriptionStatus.EXEMPT;
        } else if (string.equalsIgnoreCase("done")) {
            return ConscriptionStatus.DONE;
        } else if (string.equalsIgnoreCase("pending")) {
            return ConscriptionStatus.PENDING;
        }else if (string.equalsIgnoreCase("serving")) {
            return ConscriptionStatus.SERVING;
        } else {
            return ConscriptionStatus.NONE;
        }
    }
}
