package main.java.model;

import com.opencsv.CSVReader;
import java.io.FileReader;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Shahab
 */
public final class ReadFromCSV {
    private ReadFromCSV(){}
    public static Information Read(String FILE){
        Map<String,String> csvInfo = new HashMap<>();
        Information information = null;
        CSVReader csvReader = null;
        try {
            FileReader fileReader = new FileReader(FILE);
            csvReader = new CSVReader(fileReader);
            List<String[]> Record;
            Record = csvReader.readAll();
            String[] keys = Record.get(0);
            String[] values = Record.get(1);
            for (int i=0;i<keys.length;i++){
                csvInfo.put(keys[i],values[i]);
            }
            information = new Information(
                    null,
                    Calendar.getInstance().getTime(),
                    Integer.parseInt(csvInfo.get("Salary request")),
                    csvInfo.get("Address"),
                    csvInfo.get("Birth date"),
                    csvInfo.get("Description"),
                    csvInfo.get("Email"),
                    csvInfo.get("ID.NO"),
                    csvInfo.get("Name"),
                    csvInfo.get("Last Name"),
                    csvInfo.get("Mobile number"),
                    csvInfo.get("Phone number"),
                    Degree.getValue(csvInfo.get("Degree")),
                    Gender.getValue(csvInfo.get("Gender")),
                    ConscriptionStatus.getValue(csvInfo.get("Conscription status")),
                    MaritalStatus.getValue(csvInfo.get("Marriage Status"))
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return information;
    }
}
