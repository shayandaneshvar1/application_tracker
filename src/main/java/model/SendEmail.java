package main.java.model;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

public final class SendEmail {
    // === field ===
    private static final int TLS_PORT = 587;
    private static final int SSL_PORT = 465;
    private static final String host = "smtp.gmail.com";
    private static final String from = "busnotstopforyou@gmail.com";
    private static final String userName = "busnotstopforyou@gmail.com";
    private static final String password = "Aq!sw2de3";
    private static boolean authentication = true;
    private static String to;
    private static String body;
    private static String subject;
    private static String company;


    // === constructor ===
    private SendEmail() {
    }

    // === getter  ===


    public static String getCompany() {
        return company;
    }

    public static String getSubject() {
        return subject;
    }

    public static int getTLS_PORT() {
        return TLS_PORT;
    }

    public static int getSSL_PORT() {
        return SSL_PORT;
    }

    public static String getHost() {
        return host;
    }

    public static String getFrom() {
        return from;
    }

    public static String getUserName() {
        return userName;
    }

    public static String getPassword() {
        return password;
    }

    public static boolean isAuthentication() {
        return authentication;
    }

    public static String getTo() {
        return to;
    }

    public static String getBody() {
        return body;
    }

    // === setter ===


    public static void setCompany(String comp) {
        company = comp;
    }

    public static void setAuthentication(boolean auth) {
        authentication = auth;
    }

    public static void setTo(String receiver) {
        to = receiver;
    }

    public static void setBody(String emailBody) {
        body = emailBody;
    }

    public static void setSubject(String title) {
        subject = title;
    }

    // === operator ===
    public static void send_ssl_mail(String to, String subject, String body,
                                     Message.RecipientType type) {
        Properties pr = new Properties();
        pr.put("mail.host", host);
        pr.put("mail.smtp.port", SSL_PORT);

        pr.put("mail.smtp.ssl.enable", true);
        pr.put("mail.smtp.socketFactory.port", SSL_PORT);
        pr.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        Authenticator auth = null;
        if (authentication) {
            pr.put("mail.smtp.auth", true);
            auth = new Authenticator() {
                PasswordAuthentication pa = new PasswordAuthentication(userName, password);

                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return pa;
                }
            };
        }

        Session session = Session.getDefaultInstance(pr, auth);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress recipient = new InternetAddress(to);
            recipient = new InternetAddress(to);
            message.setRecipients(type, recipient.getAddress());
            message.setSubject(subject);
            message.setSentDate(new Date());
            message.setText(body);
            message.setDataHandler(new DataHandler(new HTMLDataSource(body)));
            Transport.send(message);
            System.out.println("Message has been sent");
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error!" + ex.getMessage());
        }

    }

    static class HTMLDataSource implements DataSource {

        private String html;

        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (html == null) throw new IOException("html message is null!");
            return new ByteArrayInputStream(html.getBytes());
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }

        @Override
        public String getContentType() {
            return "text/html";
        }

        @Override
        public String getName() {
            return "HTMLDataSource";
        }
    }
}
