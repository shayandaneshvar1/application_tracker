package main.java.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.java.model.Application;
import main.java.model.Document;
import main.java.model.Type;
import main.java.repository.RepositoryManager;

import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class DocumentController implements Initializable {
    private static Application application;
    private static Stage stage;

    public static Stage getStage() {
        return stage;
    }

    public static void setStage(Stage stage) {
        DocumentController.stage = stage;
    }

    @FXML
    private JFXButton addButton;
    @FXML
    private JFXTextField addressBar;
    @FXML
    private JFXComboBox<Type> typeBox;
    @FXML
    private JFXComboBox<String> docBox;
    private File selectedFile;
    private Document document;
    @FXML
    private JFXButton deleteButton;


    public static Application getApplication() {
        return application;
    }

    public static void setApplication(Application application) {
        DocumentController.application = application;
    }

    @FXML
    void editAction(ActionEvent event) {
        if (docBox.getValue() != null && document != null) {
            document.setAddress("file:///" +addressBar.getText().trim());
            document.setType(typeBox.getValue());
            RepositoryManager.getInstance().getDocumentRepository().update(document);
            stage.close();
        }else {
            Toolkit.getDefaultToolkit().beep();
        }
        document = null;
    }

    @FXML
    void delete(ActionEvent event) {
        if(docBox.getValue() != null && document != null){
            RepositoryManager.getInstance().getDocumentRepository().delete(document.getID());
            stage.close();
        }else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        document = null;
        typeBox.getItems().addAll(Type.values());
        docBox.getItems().addAll(application.getDocuments().stream().map(x -> x.getType() + " => " +
                x.getAddress()).collect(Collectors.toList()));
        docBox.setOnMouseReleased(ev -> {
            if (docBox.getValue() != null) {
                document = application.getDocuments().stream().filter(x -> x.getAddress().equals(docBox.
                        getValue().split("=>")[1].trim())).findFirst().get();
                addressBar.setText(document.getAddress());
                typeBox.setValue(document.getType());
            }
        });
        addressBar.setOnMouseClicked(event -> {
            if (document != null) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setInitialDirectory(new File(document.getAddress()));
                Stage stage = new Stage();
                selectedFile = fileChooser.showOpenDialog(stage);
                if (selectedFile != null) {
                    addressBar.setText(selectedFile.getPath());
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }
}
