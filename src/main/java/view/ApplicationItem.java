package main.java.view;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import main.java.model.Application;
import main.java.model.Export;
import main.java.model.Type;

import java.io.File;
import java.io.IOException;

/**
 * @author Shayan
 */
public class ApplicationItem extends StackPane {
    private static ApplicationItem focus = null;
    private static Stage infoStage;
    private static Stage interviewStage;// TODO: 8/19/19  
    private Rectangle mainItem;
    private ImageView frame;
    private ImageView print;
    private Integer width;
    private Integer height;
    private Label status;
    private Application application;
    private ScrollPane tagPane;
    private FlowPane tagContainer;

    public ApplicationItem(Integer width, Integer height, Application application) {
        print = new ImageView(new Image("main/resources/print.png"));
        this.application = application;
        this.height = height;
        this.width = width;
        setAlignment(Pos.CENTER);
        setWidth(width);
        setHeight(height);
        mainItem = new Rectangle(width, height);
        getChildren().add(mainItem);
        mainItem.setFill(Color.web("359768"));
        Text text = new Text(application.getInformation().getFullname());
        text.setStyle("-fx-text-fill: White;-fx-font-size: 18px;-fx-fill: " +
                "White");
        getChildren().add(text);
        tagPane = new ScrollPane();
        tagContainer = new FlowPane();
        tagPane.setId("tagRelated");
        tagContainer.setId("tagRelated");
        tagPane.setContent(tagContainer);
        application.getTags().forEach(x -> tagContainer.getChildren().add(labelMaker(x.getTitle())));
        tagPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        tagPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        if (application.getTags().size() == 0) {
            tagPane.setVisible(false);
        }
//        tagPane.setId("error");
        tagPane.setTranslateY(42);
        tagPane.setTranslateX(20);
        tagPane.setMaxWidth(244);
        tagPane.setMaxHeight(50);
        tagContainer.setHgap(4d);


        getChildren().add(tagPane);


        text.setTranslateY(-48);
        print.setTranslateX(width / 2 - width / 7);
        print.setTranslateY(height / 2 - height / 4);
        print.setTranslateY(height / 2 - height / 4);
        getChildren().add(print);
        mainItem.setArcHeight(height / 5);
        mainItem.setArcWidth(height / 5);
        mainItem.setOnMouseEntered(event -> {
            focus = this;
            View.refresh();
        });

        mainItem.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                setVisible(false);
                setMaxWidth(0);
                setMaxHeight(0);
            } else if (event.getButton() == MouseButton.PRIMARY) {
                Platform.runLater(() -> {
                    InfoController.setApplication(application);

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/view/info.fxml"));
                    try {
                        AnchorPane anchorPane = loader.load();
                        Scene scene = new Scene(anchorPane);
                        infoStage = new Stage();
                        infoStage.setScene(scene);
                        infoStage.initModality(Modality.WINDOW_MODAL);
                        infoStage.initOwner(View.getMainStage());
                        infoStage.initStyle(StageStyle.UNDECORATED);
                        infoStage.show();
                        infoStage.setX(infoStage.getX() + 190);
                        infoStage.setY(infoStage.getY() + 20);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

            }
        });

        print.setOnMouseClicked(event -> {
            Stage stage = new Stage();
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setInitialDirectory(new File("../../"));
            File selectedDirectory = directoryChooser.showDialog(stage);
            if (selectedDirectory != null) {
                String string;
                Export.CreateZip(application, string = directoryChooser.getInitialDirectory().getPath());
                System.out.println(string);
            }

        });
        setStatus(application.getLatestStatus() != null ?
                application.getLatestStatus().getState().toString() : "--");
        application.getDocuments().stream().filter(x -> x.getType() == Type.
                IMAGE).findAny().ifPresentOrElse(x -> this.setImage(x.getAddress
                ()), () -> setDefaultImage());
    }

    // FIXME: 8/19/19 
    public static Stage getAddInterviewStage() {
        return interviewStage;
    }

    // FIXME: 8/19/19 
    public static void setAddInterviewStage(Stage stage) {
        interviewStage = stage;
    }

    public static ApplicationItem getFocus() {
        return focus;
    }

    public static Stage getInfoStage() {
        return infoStage;
    }

    public Application getApplication() {
        return application;
    }

    private Label labelMaker(String title) {
        Label label = new Label(title);
        label.setStyle("-fx-padding: 8px;-fx-background-color: #ffed8f;" +
                "-fx-background-radius: 10%;");
        return label;
    }

    public void setImage(String address) {
        Image image = new Image(address);
        frame = new ImageView(image);
        frame.setTranslateX(-width / 3);
        frame.setFitHeight(120);
        frame.setFitWidth(120);
        getChildren().add(frame);
    }

    public void setStatus(String state) {
        status = new Label(state);
        status.setStyle("-fx-background-color: #a7d46f;" +
                "-fx-background-radius: 4%;-fx-border-radius: 4%;" +
                "-fx-font-size: 12px;-fx-text-fill: Black;-fx-padding: 10px " +
                "20px");
        getChildren().add(status);
        status.setTranslateX(width / 2 - width / 7);
        status.setTranslateY(-height / 2 + height / 4);

    }

    public void setDefaultImage() {
        setImage("main/resources/pic.png");
    }
}
