package main.java.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.java.model.Application;
import main.java.model.Document;
import main.java.model.Type;
import main.java.repository.RepositoryManager;

import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class AddDocController implements Initializable {
    private static Application application;
    @FXML
    private JFXButton addButton;
    @FXML
    private JFXTextField addressBar;
    @FXML
    private JFXComboBox<Type> typeBox;
    private File selectedFile;

    public static Application getApplication() {
        return application;
    }

    public static void setApplication(Application application) {
        AddDocController.application = application;
    }

    @FXML
    void addAction(ActionEvent event) {
        if (typeBox.getValue() != null && addressBar.getText().trim().length() > 0) {
            Document newDoc = new Document(null,"file:///" + selectedFile.getPath(), typeBox.getValue(), application.getID());
            application.addToDocs(newDoc);
            RepositoryManager.getInstance().getDocumentRepository().insert(newDoc);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        addressBar.setText("");
        typeBox.getItems().addAll(Type.values());
        addressBar.setOnMouseClicked(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("../"));
            Stage stage = new Stage();
            selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile != null) {
                addressBar.setText(selectedFile.getPath());
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
        });
    }
}
