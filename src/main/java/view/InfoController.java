package main.java.view;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import main.java.model.*;
import main.java.repository.RepositoryManager;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.*;

public class InfoController implements Initializable {
    private static Application application;
    private Boolean addStatus;
    private GeneralStatus newStatus;

    @FXML
    private JFXTextField first_name;

    @FXML
    private JFXTextField last_name;

    @FXML
    private JFXTextField birth_day;

    @FXML
    private JFXTextField email;

    @FXML
    private JFXTextField tel;

    @FXML
    private JFXTextField mobile;

    @FXML
    private JFXComboBox<Gender> gender;

    @FXML
    private JFXComboBox<MaritalStatus> marital;

    @FXML
    private JFXComboBox<ConscriptionStatus> conscription;

    @FXML
    private JFXTextField address;

    @FXML
    private JFXButton submit;

    @FXML
    private JFXButton cancel;

    @FXML
    private JFXTextArea private_description;

    @FXML
    private JFXTextArea public_description;

    @FXML
    private JFXListView<String> interview;

    @FXML
    private JFXListView<String> document;

    @FXML
    private JFXComboBox<State> status_box;

    @FXML
    private JFXComboBox<Degree> degree;

    @FXML
    private JFXButton updateStatus;

    @FXML
    private JFXButton addInterview;

    @FXML
    private JFXTextField national_ssn;

    @FXML
    private JFXTextField salary;

    @FXML
    private JFXButton addDocument;

    @FXML
    private JFXButton tagSetting;

    @FXML
    private JFXTextArea description;
    @FXML
    private ImageView sendEmail;
    @FXML
    private JFXButton editInterview;

    @FXML
    private JFXButton editDocument;

    public static Application getApplication() {
        return application;
    }

    public static void setApplication(Application application) {
        InfoController.application = application;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        newStatus = application.getLatestStatus();
        addStatus = false;
        status_box.setOnMouseClicked(mouseEvent -> {
            if (status_box.getValue() != null) {
                loadData(application.getStatuses().stream().filter(x -> x.getState().toString().
                        equalsIgnoreCase(status_box.getValue().toString())).findFirst());
            }
        });
        updateStatus.setOnMouseClicked(mouseEvent -> {
            addStatus = true;
            newStatus = new GeneralStatus(null, null, application.getID(),
                    Calendar.getInstance().getTime(), null, null);

            loadData(newStatus);
        });
        loadData(application.getLatestStatus());
        loadStatuses();
        cancel.setOnMouseClicked(event -> {
            terminate();
        });
        submit.setOnMouseClicked(e -> {
            if (addStatus) {
                if (status_box.getValue() != null && private_description.getText() != null
                        && private_description.getText().length() > 0) {
                    status_box.setId("");
                    private_description.setId("");
                    updateStatus();
                    RepositoryManager.getInstance().getStatusRepository().insert(newStatus);
                    newStatus.setID(RepositoryManager.getInstance().getStatusRepository().getKey(newStatus));
                    application.addToStatuses(newStatus);
                    RepositoryManager.getInstance().getStatusRepository().insert(newStatus);

                } else {
                    status_box.setId("error");
                    private_description.setId("error");
                }
            } else {
                application.getInformation().setAddress(address.getText());
                application.getInformation().setConscriptionStatus(conscription.getValue());
                application.getInformation().setGender(gender.getValue());
                application.getInformation().setDegree(degree.getValue());
                application.getInformation().setFirstName(first_name.getText());
                application.getInformation().setLastName(last_name.getText());
                application.getInformation().setEmail(email.getText());
                application.getInformation().setDate(Calendar.getInstance().getTime());
                application.getInformation().setMobile(mobile.getText());
                application.getInformation().setTelephone(tel.getText());
                application.getInformation().setMaritalStatus(marital.getValue());
                application.getInformation().setIDNumber(national_ssn.getText());
                RepositoryManager.getInstance().getInformationRepo().update(application.getInformation());
                updateStatus();
                if (newStatus != null) {
                    RepositoryManager.getInstance().getStatusRepository().update(newStatus);
                    application.getStatuses().add(newStatus);
                }
            }
            terminate();
        });
        editDocument.setOnMouseClicked(x -> {
            FXMLLoader loaderInterview = new FXMLLoader(getClass().getResource("/main/java/view/doc.fxml"));
            DocumentController.setApplication(application);
            try {
                Stage stage = new Stage(StageStyle.UTILITY);
                DocumentController.setStage(stage);
                AnchorPane editDoc = loaderInterview.load();
                Scene scene = new Scene(editDoc);
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        addDocument.setOnMouseClicked(x -> {
            FXMLLoader loaderInterview = new FXMLLoader(getClass().getResource("/main/java/view/addDoc.fxml"));
            AddDocController.setApplication(application);
            try {
                Stage stage = new Stage(StageStyle.UTILITY);
                AnchorPane addPane = loaderInterview.load();
                Scene scene = new Scene(addPane);
                stage.setScene(scene);
                stage.show();
                System.err.println("launched");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        addInterview.setOnMouseClicked(e -> {
            InterviewController.setApplication(application);
            FXMLLoader loaderInterview = new FXMLLoader(getClass().getResource("/main/java/view/interview.fxml"));
            try {
                Stage stage = new Stage(StageStyle.UTILITY);
                AnchorPane paneInterview = loaderInterview.load();
                Scene sceneInterview = new Scene(paneInterview);
                ApplicationItem.setAddInterviewStage(stage);
                stage.setScene(sceneInterview);
                stage.show();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        tagSetting.setOnMouseClicked(event -> {
            AppTagController.setApplication(application);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/main" +
                    "/java/view/appTag.fxml"));
            try {
                AnchorPane anchorPane = loader.load();
                Scene scene = new Scene(anchorPane);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(ApplicationItem.getInfoStage());
                stage.initStyle(StageStyle.UTILITY);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        sendEmail.setOnMouseClicked(mouseEvent -> {
//            Application.sendEmail(application,);// TODO: 8/19/19  
        });
    }

    private void terminate() {
        addStatus = false;
        ApplicationItem.getInfoStage().close();
    }

    private void updateStatus() {
        if (newStatus != null) {
            newStatus.setPrivateDescriptions((private_description.getText() != null
                    && private_description.getText().length() > 0) ? private_description.getText() : "");
            newStatus.setPublicDescriptions((public_description.getText() != null
                    && public_description.getText().length() > 0) ? public_description.getText() : " ");
            newStatus.setState(status_box.getValue());
        }
    }

    public void setNewStatus(GeneralStatus newStatus) {
        this.newStatus = newStatus;
    }

    private void loadData(GeneralStatus status) {
        prepareScene();
        initialInterviewList();
        initialDocumentList();
        public_description.setText(status != null ? status.getPublicDescriptions() : "");
        private_description.setText(status != null ? status.getPrivateDescriptions() : "");
        status_box.setValue(status != null ? status.getState() : State.NONE);
        gender.setValue(application.getInformation().getGender());
        marital.setValue(application.getInformation().getMaritalStatus());
        conscription.setValue(application.getInformation().getConscriptionStatus());
        degree.setValue(application.getInformation().getDegree());
        first_name.setText(application.getInformation().getFirstName());
        last_name.setText(application.getInformation().getLastName());
        tel.setText(application.getInformation().getTelephone());
        mobile.setText(application.getInformation().getMobile());
        address.setText(application.getInformation().getAddress());
        email.setText(application.getInformation().getEmail());
        birth_day.setText(application.getInformation().getBirthDate());
        national_ssn.setText(application.getInformation().getIDNumber());
        description.setText(application.getInformation().getDescription());
        salary.setText(String.valueOf(application.getInformation().getReqSalary()));
        setNewStatus(status);

    }

    private void initialInterviewList() {
        List<Interview> list = RepositoryManager.getInstance().getInterviewRepo().searchInterviewByAppId(application.getID());
        if (application.getInterviews().size() > 0) {
            list.forEach(x -> interview.getItems().add(x.getSubject() + "   " + x.getInterViewer()));
        }
    }

    private void initialDocumentList() {
        List<Document> list = RepositoryManager.getInstance().
                getDocumentRepository().selectByAppID(application.getID());
        Map<Document, String> documentStringMap = new HashMap<>();
        if (application.getDocuments().size() > 0) {
            list.stream().forEach(x -> documentStringMap.put(x,
                    x.getType().toString() + "   " + x.getAddress()));
            documentStringMap.forEach((x, y) -> document.getItems().add(y));
            document.setOnMouseClicked(event -> {
                documentStringMap.entrySet().forEach(x -> {
                    if (document.getSelectionModel().getSelectedItems().get(0).
                            equals(x.getValue())) {
                        File file = new File(x.getKey().getAddress().split(
                                "file:///")[1]);
                        Desktop desktop = Desktop.getDesktop();
                        try {
                            desktop.open(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            });
        }
    }

    private void loadData(Optional<GeneralStatus> status) {
        if (status.isPresent()) {
            newStatus = status.get();
            loadData(status.get());
        }
    }

    private void prepareScene() {

        gender.getItems().clear();
        marital.getItems().clear();
        conscription.getItems().clear();
        degree.getItems().clear();
        status_box.getItems().clear();

        gender.getItems().addAll(Gender.values());
        marital.getItems().addAll(MaritalStatus.values());
        conscription.getItems().addAll(ConscriptionStatus.values());
        degree.getItems().addAll(Degree.values());

        status_box.getItems().addAll(State.values());

    }

    private void loadStatuses() {
        if (application.getStatuses().size() > 0) {
            for (GeneralStatus status : application.getStatuses()) {
                status_box.getItems().add(status.getState());
            }
        }
    }

}
