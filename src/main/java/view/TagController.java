package main.java.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import main.java.model.Tag;
import main.java.repository.RepositoryManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class TagController implements Initializable {

    @FXML
    private JFXTextField tagfField;

    @FXML
    private JFXButton addButton;

    @FXML
    private JFXButton deleteButton;

    @FXML
    private ListView<String> tagList;

    @FXML
    void addAction(ActionEvent event) {
        String str = tagfField.getText();
        List<String> strings = tagList.getItems();
        if (strings.stream().noneMatch(x -> x.equalsIgnoreCase(str)) && str.
                length() > 0) {
            tagList.getItems().add(str);
            RepositoryManager.getInstance().getTagRepository().insert(
                    new Tag(null, str));
            tagfField.setId("");
        } else {
            tagfField.setId("error");
        }
    }

    @FXML
    void deleteAction(ActionEvent event) {
        String str = tagfField.getText();
        List<String> strings = tagList.getItems();
        if (strings.stream().anyMatch(x -> x.equals(str))) {
            tagList.getItems().remove(str);
            Integer id = RepositoryManager.getInstance().getTagRepository().
                    getKey(new Tag(null, str));
            RepositoryManager.getInstance().getTagRepository().delete(id);
            tagfField.setId("");
        } else {
            tagfField.setId("error");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        List<Tag> tags = RepositoryManager.getInstance().getTagRepository()
                .selectAll();
        tagList.getItems().addAll(tags.stream().map(x -> x.getTitle()).collect(
                Collectors.toList()));
    }
}
