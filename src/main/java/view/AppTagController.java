package main.java.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import main.java.model.Application;
import main.java.model.Tag;
import main.java.repository.RepositoryManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class AppTagController implements Initializable {
    private static Application application;
    private List<Tag> tags;

    public static Application getApplication() {
        return application;
    }

    public static void setApplication(Application application) {
        AppTagController.application = application;
    }

    @FXML
    private JFXButton addButton;

    @FXML
    private JFXButton deleteButton;

    @FXML
    private ListView<String> tagList;

    @FXML
    private JFXComboBox<String> tagBox;

    @FXML
    void addAction(ActionEvent event) {
        if (tagBox.getValue() != null && tagList.getItems().stream().noneMatch(x -> x.equalsIgnoreCase(tagBox.getValue()))) {
            tagList.getItems().add(tagBox.getValue());
            Tag newTag = tags.stream().filter(x -> x.getTitle().
                    equalsIgnoreCase(tagBox.getValue())).findFirst().get();
            application.addToTags(newTag);
            RepositoryManager.getInstance().getTagbankDAO().insert(newTag,application);
        }
    }

    @FXML
    void deleteAction(ActionEvent event) {
        if (tagBox.getValue() != null && tagList.getItems().stream().anyMatch(x -> x.equalsIgnoreCase(tagBox.getValue()))) {
            tagList.getItems().remove(tagList.getItems().stream().filter(x->x.equalsIgnoreCase(tagBox.getValue())).findFirst().get());
            Tag newTag = tags.stream().filter(x -> x.getTitle().
                    equalsIgnoreCase(tagBox.getValue())).findFirst().get();
            application.removeTag(newTag);
            RepositoryManager.getInstance().getTagbankDAO().delete(newTag,
                    application);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        tags = RepositoryManager.getInstance().getTagRepository()
                .selectAll();
        tagBox.getItems().addAll(tags.stream().map(x -> x.getTitle()).collect(
                Collectors.toList()));

        tagList.getItems().addAll(application.getTags().stream().map(x -> x.getTitle()).collect(
                Collectors.toList()));
    }
}
