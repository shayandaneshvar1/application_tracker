package main.java.view;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import main.java.model.*;
import main.java.repository.RepositoryManager;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Shayan
 */
public class ViewController implements Initializable {

    private static VBox applicationItemsContainer = new VBox();
    private static List<ApplicationItem> applicationItems = new ArrayList<>();
    private static ApplicationItem focus;
    @FXML
    private JFXButton filterButton;
    @FXML
    private StackPane buttonBar;
    @FXML
    private AnchorPane root;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private Pane activityPane;
    @FXML
    private Pane filterPane;
    @FXML
    private JFXTextField searchbar;
    @FXML
    private ImageView searchIcon;
    @FXML
    private StackPane searchPane;
    @FXML
    private JFXTextArea summary;
    @FXML
    private JFXButton addButton;
    @FXML
    private JFXComboBox<String> tagBox;

    @FXML
    private JFXComboBox<Degree> degreeBox;

    @FXML
    private JFXComboBox<MaritalStatus> maritalBox;

    @FXML
    private JFXComboBox<ConscriptionStatus> conscriptionBox;

    @FXML
    private JFXComboBox<State> statusBox;

    @FXML
    private JFXToggleButton toggleSort;
    @FXML
    private JFXHamburger moreButton;

    public static VBox getApplicationItemsContainer() {
        return applicationItemsContainer;
    }

    public static List<ApplicationItem> getApplicationItems() {
        return applicationItems;
    }

    public static void handleFocus() {
        if (ApplicationItem.getFocus() != null) {
            focus = ApplicationItem.getFocus();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        focus = null;
        filterPane.setVisible(false);
        summary.setTranslateY(-180);
        System.out.println(buttonBar.getWidth());
        buttonBar.layoutXProperty().bind(root.widthProperty().divide(2).add(380));
        scrollPane.hminProperty().bind(root.heightProperty());
        scrollPane.minHeightProperty().bind(root.heightProperty().subtract(40));
        scrollPane.prefViewportHeightProperty().bind(root.heightProperty());
        scrollPane.layoutXProperty().bind(root.widthProperty().divide(4).subtract(304));
        searchPane.layoutXProperty().bind(root.widthProperty().divide(2).subtract(56));
        activityPane.layoutXProperty().bind(root.widthProperty().divide(2).subtract(56));
        activityPane.minHeightProperty().bind(root.heightProperty().subtract(100));
        summary.minHeightProperty().bind(root.heightProperty().subtract(100));
        applicationItemsContainer.setSpacing(20);
        scrollPane.setContent(applicationItemsContainer);
        applicationItemsContainer.setAlignment(Pos.CENTER);
        scrollPane.setFitToWidth(true);
        scrollPane.setPannable(true);
        degreeBox.getItems().addAll(Degree.values());
        maritalBox.getItems().addAll(MaritalStatus.values());
        conscriptionBox.getItems().addAll(ConscriptionStatus.values());
        statusBox.getItems().addAll(State.values());
        loadTags();
        scrollPane.setStyle("-fx-background-color:#ffed8f;-fx-background: #ffed8f");
        RepositoryManager.getInstance().getLatestApplications().forEach(x -> applicationItems.add(new ApplicationItem(530, 180,
                x)));
        refresh();
        scrollPane.setOnMouseClicked(event -> {
            handleMouseEvent(event);

        });
        scrollPane.setOnMouseMoved(event -> {
            refreshDescription();
        });
        searchIcon.setOnMouseClicked(event -> {
            handleSearch();
            applicationItems.forEach(x -> Arrays.stream(x.getApplication().getTags().toArray()).forEach(System.out::println));
        });
        searchbar.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                handleSearch();
            }
        });

        toggleSort.setOnMouseClicked(event -> {
            Collections.reverse(applicationItems);
            refresh();
        });

        moreButton.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/main" +
                    "/java/view/tag.fxml"));
            try {
                AnchorPane anchorPane = loader.load();
                Scene scene = new Scene(anchorPane);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(View.getMainStage());
                stage.initStyle(StageStyle.UTILITY);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void loadTags() {
        List<Tag> tags = RepositoryManager.getInstance().getTagRepository().selectAll();
        if (tags.isEmpty() || null == tags) {
            return;
        }
        tagBox.getItems().addAll(tags.stream().map(x -> x.getTitle()).collect(
                Collectors.toList()));
    }

    private void handleSearch() {
        applicationItems.clear();
        List<Application> applications = new ArrayList<>();
        if (searchbar.getText().trim().length() < 1) {
            applications.addAll(RepositoryManager.getInstance().
                    getApplicationRepo().selectAll());
        } else {
            applications.addAll(RepositoryManager.getInstance().
                    generalSearch(searchbar.getText().split(" ")));
        }
        if (filterPane.isVisible()) {
            if (conscriptionBox.getValue() != null) {
                applications = applications.stream().filter(x -> x.
                        getInformation().getConscriptionStatus() ==
                        conscriptionBox.getValue()).collect(Collectors.toList());
            }
            if (degreeBox.getValue() != null) {
                applications = applications.stream().filter(x -> x.
                        getInformation().getDegree() == degreeBox.getValue())
                        .collect(Collectors.toList());
            }
            if (maritalBox.getValue() != null) {
                applications = applications.stream().filter(x -> x.
                        getInformation().getMaritalStatus() == maritalBox.
                        getValue()).collect(Collectors.toList());
            }
            if (statusBox.getValue() != null) {
                applications = applications.stream().filter(x -> x.getStatuses()
                        .size() > 0).filter(x -> x.getLatestStatus().getState()
                        == statusBox.getValue()).collect(Collectors.toList());
            }
            if (tagBox.getValue() != null) {
                System.err.println("Called");
                applications =
                        applications.stream().filter(x -> x.getTags().stream().
                                anyMatch(y -> y.getTitle().equalsIgnoreCase(
                                        tagBox.getValue()))).collect(Collectors.toList());
                System.out.println("=========================");
                applications.forEach(x -> {
                    x.getTags().forEach(System.out::println);
                    System.out.println("--------------");
                    System.out.println(x.getInformation().getFirstName());
                });
                System.out.println("=========================");
            }
        }
        applications.forEach(x -> applicationItems.add(new ApplicationItem(530, 180, x)));
        refresh();
    }

    private void activateDefaultActivityPane() {
        activityPane.getChildren().clear();
        activityPane.getChildren().addAll(filterPane, summary);
    }

    private void refreshDescription() {
        if (focus != null && focus.getApplication().getStatuses().size() > 0) {
            summary.setText(focus.getApplication().getLatestStatus().getPrivateDescriptions());
        }
    }

    private void handleMouseEvent(MouseEvent event) {
        if (event.getButton() == MouseButton.SECONDARY) {
            handleHiding();
        } else if (event.getButton() == MouseButton.PRIMARY) {
            handleFocus();
        }
    }

    public static void handleHiding() {
        applicationItemsContainer.getChildren().clear();
        applicationItems.forEach(x -> {
            if (x.isVisible()) {
                applicationItemsContainer.getChildren().add(x);
            }
        });
    }

    public void refresh() {
        handleHiding();
        handleFocus();
        tagBox.getItems().clear();
        loadTags();
    }

    public void resetInputs() {
        applicationItems.clear();
    }

    @FXML
    void addApplication(MouseEvent event) {
        FlowPane pane = new FlowPane();
        pane.setPadding(new Insets(60));
        pane.setHgap(18);
        pane.setVgap(16);
        pane.setAccessibleHelp("Add An Application");

        activityPane.getChildren().clear();
        activityPane.getChildren().add(pane);
        TextField name = textFieldMaker("First name :", 178);
        TextField lastname = textFieldMaker("Last name :", 178);
        TextField ID = textFieldMaker("ID Number :", 114);
        TextField birthdate = textFieldMaker("Birth date :", 110);
        TextField mobile = textFieldMaker("Mobile :", 124);
        TextField email = textFieldMaker("Email :", 236);
        TextField tel = textFieldMaker("Telephone : ", 140);
        TextField salary = textFieldMaker("Salary :", 180);

        TextArea address = new TextArea();
        TextArea desc = new TextArea();
        address.setWrapText(true);
        desc.setWrapText(true);
        address.setMinWidth(450);
        address.setPrefWidth(510);
        desc.setMinWidth(450);
        desc.setPrefWidth(510);
        address.setPrefHeight(80);
        address.setMinHeight(60);
        desc.setPrefHeight(150);
        desc.setMinHeight(100);

        ComboBox<String> gender = new ComboBox<>();
        gender.setPromptText("Gender");
        gender.getItems().addAll(Arrays.stream(Gender.values()).map(x -> x.
                toString().toLowerCase()).collect(Collectors.toList()));
        ComboBox<String> degree = new ComboBox<>();
        degree.setPromptText("Degree");
        degree.getItems().addAll(Arrays.stream(Degree.values()).map(x -> x.
                toString().toLowerCase()).collect(Collectors.toList()));
        ComboBox<String> marital = new ComboBox<>();
        marital.setPromptText("Marital Status");
        marital.getItems().addAll(Arrays.stream(MaritalStatus.values()).map(x ->
                x.toString().toLowerCase()).collect(Collectors.toList()));
        ComboBox<String> conscription = new ComboBox<>();
        conscription.setPromptText("Conscription Status");
        conscription.getItems().addAll(Arrays.stream(ConscriptionStatus.values()
        ).map(x -> x.toString().toLowerCase()).collect(Collectors.toList()));

        JFXButton button = new JFXButton("Load From CSV");
        JFXButton add = new JFXButton("Add Application");
        JFXButton cancel = new JFXButton("Cancel");

        address.setPromptText("Address :");
        desc.setPromptText("Descriptions :");
        pane.getChildren().add(name);
        pane.getChildren().add(lastname);
        pane.getChildren().add(ID);
        pane.getChildren().add(birthdate);
        pane.getChildren().add(mobile);
        pane.getChildren().add(email);
        pane.getChildren().add(tel);
        pane.getChildren().add(salary);
        pane.getChildren().add(button);

        pane.getChildren().add(gender);
        pane.getChildren().add(degree);
        pane.getChildren().add(marital);
        pane.getChildren().add(conscription);

        pane.getChildren().add(address);
        pane.getChildren().add(desc);
        pane.getChildren().add(cancel);
        pane.getChildren().add(add);
        button.setMinWidth(150);
        add.setMinWidth(250);
        button.setMinWidth(150);
        cancel.setMinWidth(120);
        button.setId("filter");
        button.setTextFill(Color.WHITE);
        cancel.setId("cancel");
        add.setId("submit");

        cancel.setOnMouseClicked(event1 -> {
            activateDefaultActivityPane();
        });

        button.setOnMouseClicked(event1 -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
                    "Comma-separated values", "*.csv"));
            fileChooser.setInitialDirectory(new File("../../"));
            Stage stage = new Stage();
            File selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile != null) {
                Information info = ReadFromCSV.Read(selectedFile.getPath());
                name.setText(info.getFirstName());
                lastname.setText(info.getLastName());
                mobile.setText(info.getMobile());
                email.setText(info.getEmail());
                marital.setValue(info.getMaritalStatus().toString());
                degree.setValue(info.getDegree().toString());
                desc.setText(info.getDescription());
                address.setText(info.getAddress());
                conscription.setValue(info.getConscriptionStatus().toString());
                gender.setValue(info.getGender().toString());
                tel.setText(info.getTelephone());
                salary.setText(String.valueOf(info.getReqSalary()));
                ID.setText(info.getIDNumber());
                birthdate.setText(info.getBirthDate());
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
        });
        add.setOnMouseClicked(event1 -> {
            try {
                if (name.getText().trim().length() > 2 && lastname.getText().trim().
                        length() > 2 && email.getText().matches("^[\\w-_\\.+]*" +
                        "[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$") && gender.getValue
                        () != null && marital.getValue() != null && !salary.getText
                        ().chars().anyMatch(x -> Character.isAlphabetic(x))) {
                    Information information = new Information(null,
                            Calendar.getInstance().getTime(), Integer.parseInt(
                            salary.getText()), address.getText(), birthdate.getText
                            (), desc.getText(), email.getText(), ID.getText(), name.
                            getText(), lastname.getText(), mobile.getText(), tel.
                            getText(), Degree.getValue(degree.getValue()),
                            Gender.getValue(gender.getValue()), ConscriptionStatus
                            .getValue(conscription.getValue()), MaritalStatus.
                            getValue(marital.getValue()));
                    Application application = new Application(null, information);
                    RepositoryManager.getInstance().getInformationRepo().insert(information);
                    information.setID(RepositoryManager.getInstance().
                            getInformationRepo().getKey(information));
                    RepositoryManager.getInstance().getApplicationRepo().insert(application);
                    activateDefaultActivityPane();
                } else {
                    Toolkit.getDefaultToolkit().beep();
                    System.out.println("beep");
                    name.setId("error");
                    lastname.setId("error");
                    email.setId("error");
                    degree.setId("error");
                    gender.setId("error");

                    if (name.getText().trim().length() > 2) {
                        name.setId("");
                    }
                    if (lastname.getText().trim().
                            length() > 2) {
                        lastname.setId("");
                    }
                    if (email.getText().matches("^[\\w-_\\.+]*" +
                            "[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$")) {
                        email.setId("");
                    }
                    if (degree.getValue() != null) {
                        degree.setId("");
                    }
                    if (marital.getValue() != null) {
                        marital.setId("");
                    }
                    if (!salary.getText().chars().anyMatch(x -> Character.
                            isAlphabetic(x))) {
                        salary.setId(" ");
                    }
                }
            } catch (Exception ex) {
                Toolkit.getDefaultToolkit().beep();
                add.setText("Try Again");
                ex.printStackTrace();
            }
        });
    }

    private TextField textFieldMaker(String prompt) {
        TextField textField = new TextField();
        textField.setPromptText(prompt);
        return textField;
    }

    private TextField textFieldMaker(String prompt, Integer width) {
        TextField textField = textFieldMaker(prompt);
        textField.setPrefWidth(width);
        return textField;
    }

    @FXML
    void handleFilter(MouseEvent event) {
        refresh();
        activateDefaultActivityPane();
        if (filterPane.isVisible()) {
            maritalBox.setValue(null);
            statusBox.setValue(null);
            degreeBox.setValue(null);
            conscriptionBox.setValue(null);
            tagBox.setValue(null);
            filterPane.setVisible(false);
            summary.setTranslateY(-180);
        } else {
            summary.setTranslateY(-20);
            filterPane.setVisible(true);
        }
    }
}
