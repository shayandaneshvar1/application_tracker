package main.java.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import main.java.model.Application;
import main.java.model.Event;
import main.java.model.Interview;
import main.java.model.InterviewStatus;
import main.java.repository.RepositoryManager;

import java.net.URL;
import java.util.ResourceBundle;

public class InterviewController implements Initializable {

    private static Application application;

    @FXML
    private TextArea description;

    @FXML
    private JFXTextField interviewer;

    @FXML
    private JFXTextField subject;

    @FXML
    private JFXTextField date;

    @FXML
    private JFXComboBox<InterviewStatus> interview_status;

    @FXML
    private JFXButton apply;

    @FXML
    private JFXButton caancel;


    public static Application getApplication() {
        return application;
    }

    public static void setApplication(Application application) {
        InterviewController.application = application;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        interview_status.getItems().addAll(InterviewStatus.values());
        apply.setOnMouseClicked(e->{
          if (interview_status.getValue() != null && interviewer.getText().length()>0
                  && subject.getText().length()>0 && date.getText().length()>0 ){
              interview_status.setId("");
              subject.setId("");
              date.setId("");
              interviewer.setId("");
              Interview interview = new Interview(null, Event.formatDate(date.getText()),
                      interviewer.getText(),subject.getText(),
                      interview_status.getValue(),description.getText());
              getApplication().getInterviews().add(interview);
              RepositoryManager.getInstance().getInterviewRepo().insert(interview,getApplication());
              terminate();
          }else {
              interview_status.setId("error");
              subject.setId("error");
              date.setId("error");
              interviewer.setId("error");
          }

        });
        caancel.setOnMouseClicked(e->{
            terminate();
        });
    }

    private void terminate() {

        ApplicationItem.getAddInterviewStage().close();
    }
}


