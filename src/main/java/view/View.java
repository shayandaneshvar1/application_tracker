package main.java.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.java.model.Application;

import java.io.IOException;

/**
 * @author Shayan
 */
public final class View extends javafx.application.Application {
    private static Stage mainStage;
    private static boolean launched = false;

    public View() {
    }

    @Override
    public void start(Stage stage) throws Exception {
        mainStage = stage;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/main" +
                    "/java/view/mainMenu.fxml"));
            AnchorPane anchorPane = loader.load();
            Scene scene = new Scene(anchorPane);
            stage.setScene(scene);
            stage.setFullScreen(false);
            stage.setMinHeight(768);
            stage.setMinWidth(1280);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Stage getMainStage() {
        return mainStage;
    }

    public void insertApplication(Application application) {
        ApplicationItem item = new ApplicationItem(530, 180, application);
        ViewController.getApplicationItems().add(item);
        ViewController.handleHiding();
    }

    public static void refresh() {
        ViewController.handleFocus();
        ViewController.handleHiding();
    }

    public void launcher() {
        if (!launched) {
            launched = true;
            launch();
        }
    }
}
