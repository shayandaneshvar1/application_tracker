package test.java;

import main.java.model.*;
import main.java.repository.ApplicationRepository;
import main.java.repository.InformationRepository;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Test {
    public static final String DB_URL = "jdbc:mysql://localhost:3306/hire";
    public static final String USER_NAME = "root";
    public static final String PASSWORD = "123456";
//
    private Connection connection;
//
//    private ApplicationRepository applicationRepository;
    private InformationRepository informationRepository;
//
    Test(){
        try {
            connection = DriverManager.getConnection(DB_URL,USER_NAME,PASSWORD);
//           applicationRepository = new ApplicationRepository(connection);
            informationRepository = new InformationRepository(connection);

            testDatabase();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main(String[] args) {
//        Arrays.stream(Gender.values()).forEach(System.out::println);
        Test test = new Test();
        test.testDatabase();
    }

    private void testDatabase(){


        java.util.Date date=new java.util.Date();
        List<Information> informationList = new ArrayList<>();
        for (int i=0 ; i<10 ; i++){
            Information in = new Information(0,date,i,"addres"+i,"birthdata"+i,
                    "description"+i,"gmail"+i,"100000"+i,"A"+i,"B"+i,
                    "0912000000"+i,"021000000"+i, Degree.BACHELORS, Gender.FEMALE, ConscriptionStatus.DONE,MaritalStatus.MARRIED);
            informationList.add(in);
        }

        informationList.stream().forEach(x->informationRepository.insert(x));
//        informationRepository.selectAll().stream().forEach(x->System.out.println(x.toString()));
//        informationRepository.update(new Information(1,new Date(),200,"CCC","1234","qwerr",
//                "ssssss","09090","kamal","karimi","00000","00000",Degree.BACHELORS,Gender.FEMALE,ConscriptionStatus.DONE,MaritalStatus.SINGLE));
//        informationRepository.selectAll().stream().forEach(x->System.out.println(x.toString()));
//        List<Application> applications = new ArrayList<>();
//        for (int i=0;i<10;i++){
//            Application application = new Application(null,i,date.toString());
//            applications.add(application);
//        }

//       applications.stream().forEach(x->applicationRepository.insert(x));
//        applicationRepository.selectAll().forEach(x->System.out.println(x.toString()));
//        Application app = new Application(6,100,date.toString());
//        applicationRepository.update(app);
//        applicationRepository.selectAll().forEach(x->System.out.println(x.toString()));
//        System.out.println("___________________________________________________________________________");
//        applicationRepository.searchByID(6).stream().forEach(x->System.out.println(x.toString()));
//        applicationRepository.searchByInfoID(6).stream().forEach(x->System.out.println(x.toString()));
    }
}
