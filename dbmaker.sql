create DATABASE hire;
use hire;


create table information
(
    ID                  int primary key auto_increment,
    modified            varchar(100),
    salary              int,
    address             varchar(250),
    birth_date          varchar(100),
    description         varchar(250),
    email               varchar(100),
    ID_number           varchar(50),
    first_name          varchar(90),
    last_name           varchar(90),
    mobile              varchar(40),
    telephone           varchar(30),
    degree              enum ('DIPLOMA', 'ASSOCIATES', 'BACHELORS', 'MASTERS', 'PhD', 'NONE'),
    gender              enum ('MALE','FEMALE','OTHER'),
    conscription_status enum ('PENDING','EXEMPT','DONE','SERVING'),
    marital_status      enum ('SINGLE','MARRIED')
);

#  drop table information;
create table tag
(
    ID    int primary key auto_increment not null,
    title varchar(100) unique            not null
);
create table tagBank
(
    tag_ID        int ,
    applicationID int ,
    constraint primary key (tag_ID, applicationID)
);
# drop table tagBank;
create table application
(
    ID            int primary key auto_increment,
    info_ID       int references information (ID),
    creation_date varchar(100)
);

# drop table application;
create table document
(
    ID       int primary key auto_increment,
    url      varchar(250) unique,
    doc_type enum ('CV','ID','IMAGE','OTHER','NONE'),
    app_ID   int references application (ID)
);

# INSERT into document values(null,'main/resources/download.jpeg','IMAGE',1);

# use hire;


create table interview
(
    id                     int auto_increment primary key,
    date_interview         varchar(100),
    interviewer            varchar(80) not null,
    subject_interview      varchar(80) not null,
    status_interview       enum ('NOT_ATTENDED','ATTENDED','MISSED','NONE'),
    point_result_interview varchar(300),
    app_id                 int         not null references application (id)

);
create table generalStatus
(
    id                  int auto_increment primary key,
    status              enum ('ADDED','INTERVIEW','PASSED','FAILED',
        'NONE'),
    app_id              int references application (id),
    creation_date       varchar(100),
    private_description varchar(250),
    public_description  varchar(250)

);

#  drop database hire;


#TODO : General Status


# INSERT into application values (1,1,'2012,2,2,13,0');
# INSERT into application values (null,2,'2012,2,2,13,0');
# INSERT into application values (null,3,'2012,2,2,13,0');
# INSERT into application values (null,1,'2012,2,2,13,0');

# INSERT into information values (1,'2013,2,1,3,12',1000,'address1','1',
#                                 'nothing','x@y.z','123','ali','alavi','0919',
#                                 '123',
#                                 'DIPLOMA','MALE','EXEMPT','SINGLE');
#
# INSERT into information values (null,'2012,2,1,3,12',12000,'address2','2',
#                                 'nothing','a@y.z','123','zahra','alavi','0919',
#                                 '1231',
#                                 'MASTERS','FEMALE','EXEMPT','SINGLE');
#
# INSERT into information values (null,'2011,2,6,3,12',12000,'addrezz2','22',
#                                 'nothing','a@y.z','123','sara','jafari','0919',
#                                 '1232',
#                                 'ASSOCIATES','FEMALE','EXEMPT','MARRIED');
# #
# INSERT into information values (null,'2012,2,1,3,12',12000,'address12','12',
#                                 'nothing','a@y.z','123','Mc','Hammer','0919',
#                                 '1223',
#                                 'PHD','MALE','EXEMPT','SINGLE');

# INSERT into generalStatus values (1,'NONE',1,'2015,1,12,3,1','ok','Come');
# INSERT into generalStatus values (null,'FAILED',2,'2011,1,12,3,1','ok',
#                                   'Come');
# INSERT into generalStatus values (null,'ADDED',3,'2012,1,12,3,1',
#                                   'ok','Come');
# INSERT into generalStatus values (null,'NONE',4,'2011,1,12,3,1','ok','Come');

# INSERT into
# use hire;
# INSERT into application values (11,9,'2019,2,2,13,0');
# INSERT into information values (9,'2019,2,1,3,12',12000,'address2','2',
#                                 'nothing','a@y.z','123','kamal','Ka','0919',
#                                 '1231',
#                                 'MASTERS','MALE','EXEMPT','SINGLE');
# INSERT into generalStatus values (9,'NONE',11,'2011,1,12,3,1','ok','Come');
# INSERT into tag values (10,'Web Assembly');
# Insert into tagBank values (10,11);


# INSERT into application values (15,14,'2019,2,2,13,0');
# INSERT into information values (14,'2019,2,1,3,12',12000,'address2','2',
#                                 'nothing','a@y.z','123','kamal','Ka','0919',
#                                 '1231',
#                                 'MASTERS','MALE','EXEMPT','SINGLE');
# INSERT into generalStatus values (30,'NONE',15,'2011,1,12,3,1','Nice Guy -
# This is a sample private description',
#                                   'Public desc test');

# INSERT into application
# values (40, 40, '2019,2,2,13,0');
# INSERT into information
# values (40, '2019,2,1,3,12', 12000, 'address2', '2',
#         'nothing', 'asdasd@y.z', '123', 'John', 'Doe',
#         '0919',
#         '1231',
#         'MASTERS', 'MALE', 'EXEMPT', 'SINGLE');
# INSERT into generalStatus
# values (40, 'ADDED', 11, '2001,1,12,3,1', 'Good', 'Nice');
# INSERT into tag
# values (40, 'Python');
# INSERT into tag
# values (41, 'Rust');
# INSERT into tag
# values (42, 'Scala');
# INSERT into tag
# values (43, 'Haskel');
# Insert into tagBank
# values (40, 40);
# Insert into tagBank
# values (41, 40);
# Insert into tagBank
# values (42, 40);
# Insert into tagBank
# values (43, 40);
